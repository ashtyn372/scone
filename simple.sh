rm -r dist

html-minifier --input-dir ./ --output-dir dist --collapse-whitespace --remove-comments --remove-optional-tags --no-html5 --remove-redundant-attributes --remove-script-type-attributes --remove-tag-whitespace --use-short-doctype --remove-tag-whitespace --minify-css true --minify-js true 

echo starting to minify css 

postcss css -d dist/css 

echo js minify 

npx webpack 

echo copying resources 

rm -r dist/favicon 

cp -r favicon dist/favicon

rm -r dist/static-assets

cp -r static-assets dist/static-assets