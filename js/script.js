
var firebaseConfig = {
    apiKey: "AIzaSyD6RD0Nf6CA0hzJbqIky4CIuaKRUubz_Yc",
    authDomain: "public-store-easy.firebaseapp.com",
    databaseURL: "https://public-store-easy.firebaseio.com",
    projectId: "public-store-easy",
    storageBucket: "public-store-easy.appspot.com",
    messagingSenderId: "568810120317",
    appId: "1:568810120317:web:f62a03b918dcc3cbf13d14",
    measurementId: "G-1FNYGZEEQN"
};  // Initialize Firebase  firebase.initializeApp(firebaseConfig);
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();



class Task {
    constructor (name, description){
        this.name = name;
        this.description = description;
    }
    toString() {
        return this.name +": "+description;
    }
}

taskConverter = {
    toFirestore: function(task){
        return{
            name: task.name,
            description: task.description
        }
    },
    fromFirestore: function(snapshot, options){
        const data = snapshot.data(options);
        return new Task(data.name,data.description)
    }
}



// stores the currently logged in user as a constant
const currentUser = getUser();

// loads pre-existing tasks from db
loadTasks();

const newBtn = document.getElementsByClassName('plus-icon')[0];



// function to get the current logged in user. 
function getUser(){
    if(localStorage.getItem('status')) {
        owner = localStorage.getItem('owner');
        console.log('User logged in: ' + owner);
        // sets current user collection
        user = db.collection('users').doc(owner);
        return user;
    } else {
        localStorage.setItem('status', 'account made');
        console.log("User not logged in.");
        window.location = '../dist-main/login.html';
    }
}

// Gets the todoList of the current user, stored as an array.
function getTaskList(user){
console.log(user);
user.get().then((data)=> {
    data = data.data();
    return data.todoList;
})

}

function display(values) {
    console.log(values);
}


// Reads tasks from the users database.
function loadTasks() {
    console.log("Loading tasks...");
    currentUser.get().then((data)=>{
        data = data.data()
        if(data.todoList == null){
            currentUser.update({
                todoList: {}
            })
        }else{
            let loadList = data.todoList;
            let list = document.getElementById('todo-nav-list');
            // into on iterating through a custom object:
            // https://zellwk.com/blog/looping-through-js-objects/
            const listKeys = Object.keys(loadList);
            for (const key of listKeys) {
                let task = document.createElement('div');
                task.innerText = key;
                let button = document.createElement('button');
                button.textContent = "Delete";
                button.value = key;
                button.addEventListener("click", () => {
                    // delete task associated with button.value
                    console.log(button.value);
                    kill(button.value);
                    
                });
                task.prepend(button);
                list.prepend(task);
            }      
            if (list.childElementCount == '1') {
                document.getElementById('tasks-stats').innerHTML = list.childElementCount + ' task';
            } else {
                document.getElementById('tasks-stats').innerHTML = list.childElementCount + ' tasks';
            }
        }
    })
}

// triggers when clicking the + button, creates a new task.
function addTask() {
    let taskPrompt = document.getElementById('task-input').value;
    taskPrompt = taskPrompt.replace(/<br\s*[\/]?>/gi, ' ');
    document.getElementById('prompt-event').style.display = 'none';

    if (taskPrompt.length <= 0) {
        console.error('blank task creation failed')
    } else {
        let task = document.createElement('div');
        let button = document.createElement('button');
        button.textContent = "Delete";
        button.value = taskPrompt;
        button.addEventListener("click", () => {
            // delete task associated with button.value
            console.log(button.value);
            kill(button.value);
        });
        task.innerText = taskPrompt;
        newTask = new Task(task.innerText,task.innerText);
        db.collection('users').doc('Mark').set({
            todoList: {
                [newTask.name]:{
                name: newTask.name,
                description: newTask.description
            }
        }
        },{merge:true});
        let list = document.getElementById('todo-nav-list');
        task.prepend(button);
        list.prepend(task);
        console.log(button.value);

        // Grabs current todoList, then adds the recently added task to the end and updates the db

        

        currentUser.get().then((data)=> {
            data = data.data()
            newList = data.todoList;
            
        })
        

        if (list.childElementCount == '1') {
            document.getElementById('tasks-stats').innerHTML = list.childElementCount + ' task';
        } else {
            document.getElementById('tasks-stats').innerHTML = list.childElementCount + ' tasks';
        }
    }
}

// deletes a task
function kill(taskName) {

    currentUser.set({
        todoList: {
            [taskName]: firebase.firestore.FieldValue.delete()
        }
    }, { merge: true }).then(() => {
    window.location.reload(true)

    })
}

// listens for a click on the (+) button.
newBtn.addEventListener('click', () => {
    document.getElementById('prompt-event').style.display = 'block';
    document.addEventListener('click', (event) => {
        if (event.target.contains(document.getElementById('prompt-event'))) {
            document.getElementById('prompt-event').style.display = 'none'
        }
    })
    let taskPrompt = document.getElementById('task-input').value = '';
    document.getElementById('action-btn').addEventListener('click', addTask);
})

//experimenting adding new button
//const delBtn = document.getElementById('delBtn');
//console.log(delBtn);
//delBtn.addEventListener('click', () => {
//    console.log("You clicked me!");
//})




